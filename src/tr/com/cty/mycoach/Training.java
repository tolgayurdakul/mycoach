package tr.com.cty.mycoach;

public class Training {

	public static String PHASE_WORKOUT = "WORKOUT";
	public static String PHASE_REST = "REST";

	public static int STATE_STOPPED = 0;
	public static int STATE_RUNNING = 1;
	public static int STATE_PAUSED = 2;
	
	private final int workoutDuration;
	private final int restingDuration;
	private int remaining;
	
	private int state;
	private String phase;

	public Training(int workoutDuration, int restingDuration) {
		this.workoutDuration = workoutDuration;
		this.restingDuration = restingDuration;
		resetTraining();
	}

	public int getWorkoutDuration() {
		return workoutDuration;
	}

	public int getRestingDuration() {
		return restingDuration;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public void advance() {
		remaining--;
	}
	
	public void resetTime() {
		if (phase == PHASE_WORKOUT) {
			remaining = getWorkoutDuration();
		} else {
			remaining = getRestingDuration();
		}
	}
	
	public int getRemainingTime() {
		return remaining;
	}

	public void resetTraining() {
		remaining = getWorkoutDuration();
		state = STATE_STOPPED;
		phase = PHASE_WORKOUT;
	}

	
}
