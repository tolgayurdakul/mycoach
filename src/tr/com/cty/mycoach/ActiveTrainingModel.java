package tr.com.cty.mycoach;

public class ActiveTrainingModel {

	static ActiveTrainingModel instance = null;
	private Training activeTraining;
	
	public static ActiveTrainingModel getInstance() {
		if (instance == null) {
			instance = new ActiveTrainingModel();
		}
		return instance;
	}
	
	public void setActiveTraining(Training training) {
		this.activeTraining = training;
	}
	
	public Training getActiveTraining() {
		return activeTraining;
	}
}
