package tr.com.cty.mycoach;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	ActiveTrainingModel activeTrainingModel = null;
	private boolean startOfPhase = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final Button buttonStart = (Button) findViewById(R.id.buttonStart);
		final Button buttonStop = (Button) findViewById(R.id.buttonStop);
		final TextView textViewRemainingTime = (TextView) findViewById(R.id.textViewRemainigTime);
		final TextView textViewActivePhase = (TextView) findViewById(R.id.textViewActivePhase);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		final SoundPool soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC,
				0);
		final int clickId = soundPool.load(this, R.raw.clock_ticking_double, 1);
		final int clickIdShort = soundPool.load(this, R.raw.clock_ticking, 1);
		final int clickIdWhistle = soundPool.load(this, R.raw.referee_whistle,
				1);

		activeTrainingModel = ActiveTrainingModel.getInstance();

		final Handler handler = new Handler();
		final Runnable runnable = new Runnable() {

			@Override
			public void run() {
				handler.postDelayed(this, 1000);

				Training activeTraining = activeTrainingModel
						.getActiveTraining();
				textViewRemainingTime.setText(activeTraining.getRemainingTime()
						+ "");
				textViewActivePhase.setText(activeTraining.getPhase());

				if (activeTraining.getPhase() == Training.PHASE_WORKOUT) {
					if (startOfPhase) {
						soundPool
								.play(clickIdWhistle, 0.99f, 0.99f, 0, 0, 1.0f);
						startOfPhase = false;
					} else
						soundPool.play(clickId, 0.99f, 0.99f, 0, 0, 1.0f);

					if (activeTraining.getRemainingTime() > 1) {
						activeTraining.advance();

					} else {
						// workout completed, transition to resting phase
						activeTraining.setPhase(Training.PHASE_REST);
						activeTraining.resetTime();
						startOfPhase = true;

					}
				} else { // PHASE_REST
					if (startOfPhase) {
						soundPool
								.play(clickIdWhistle, 0.99f, 0.99f, 0, 0, 1.0f);
						startOfPhase = false;
					} else
						soundPool.play(clickIdShort, 0.99f, 0.99f, 0, 0, 1.0f);

					if (activeTraining.getRemainingTime() > 1) {
						activeTraining.advance();

					} else {
						// resting completed, transition to workout phase
						activeTraining.setPhase(Training.PHASE_WORKOUT);
						activeTraining.resetTime();
						startOfPhase = true;
					}
				}
			}
		};

		buttonStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (activeTrainingModel.getActiveTraining().getState() == Training.STATE_STOPPED) {

					activeTrainingModel.getActiveTraining().setState(
							Training.STATE_RUNNING);
					buttonStart.setBackgroundResource(R.drawable.buttonpause);
					textViewRemainingTime.setText(activeTrainingModel
							.getActiveTraining().getRemainingTime() + "");
					textViewActivePhase.setText(activeTrainingModel
							.getActiveTraining().getPhase());

					handler.postDelayed(runnable, 1000);
					soundPool.play(clickIdWhistle, 0.9f, 0.9f, 1, 0, 1.0f);
				} else if (activeTrainingModel.getActiveTraining().getState() == Training.STATE_RUNNING) {
					handler.removeCallbacks(runnable);
					// soundPool.autoPause();
					activeTrainingModel.getActiveTraining().setState(
							Training.STATE_PAUSED);
					buttonStart.setBackgroundResource(R.drawable.buttonstart);

				} else if (activeTrainingModel.getActiveTraining().getState() == Training.STATE_PAUSED) {
					handler.postDelayed(runnable, 1000);
					// soundPool.autoResume();
					activeTrainingModel.getActiveTraining().setState(
							Training.STATE_RUNNING);
					buttonStart.setBackgroundResource(R.drawable.buttonpause);

				}
			}
		});

		buttonStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (activeTrainingModel.getActiveTraining().getState() != Training.STATE_STOPPED) {
					buttonStart.setBackgroundResource(R.drawable.buttonstart);
					activeTrainingModel.getActiveTraining().resetTraining();
					handler.removeCallbacks(runnable);
					soundPool.stop(clickId);
					soundPool.play(clickIdWhistle, 0.9f, 0.9f, 1, 0, 1.0f);
					textViewRemainingTime.setText("0");
					textViewActivePhase.setText("");
				}

			}
		});

		Training training = new Training(15, 10);
		ActiveTrainingModel.getInstance().setActiveTraining(training);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
